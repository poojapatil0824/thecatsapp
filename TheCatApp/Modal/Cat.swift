//
//  Cat.swift
//  TheCatApp
//
//  Created by Pooja Awati on 17/02/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import UIKit

// MARK: Model to structure the Cat object
class Cat: Codable {
    let breeds : [String]
    let categories : [Category]
    let id : String
    let url : URL
    
    init(breeds: [String], categories: [Category], id: String, url: URL) {
        self.breeds = breeds
        self.categories = categories
        self.id = id
        self.url = url
    }
}

class Category: Codable {
    let id : Int
    let name : String
}

