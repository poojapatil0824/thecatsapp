//
//  DetailViewController.swift
//  TheCatApp
//
//  Created by Pooja Awati on 17/02/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var catDetailedImage = UIImage()
    var catName : String = ""
    
    @IBOutlet weak var catNameLabel: UILabel!
    @IBOutlet weak var detailedImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailedImageView.image = catDetailedImage // display larger image
        catNameLabel.text = "I AM: \(catName)" // display cat id
    }
}
