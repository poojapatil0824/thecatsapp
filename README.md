#  The Cats App

###-   In the cats filter, there is no data for `clothes` and `sinks` category
###-   I have implemented `MVC` design pattern
###-   I have used `swift 4.2` and `xcode 10.1` to develop this app
###-   I have targetted `iphone X` as compatible simulator just to test
###-   There are two views: cat `list view` and `detailed view`
###-   I have implemented category filter and default selected is: `sunglasses`
###-   Images are `cached` so that it loads faster next time
###-   `Indefinite scrolling` is implemented (load data on demand) 

